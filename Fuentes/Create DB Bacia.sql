CREATE SCHEMA IF NOT EXISTS `SucursalBogota` DEFAULT CHARACTER SET utf8 ;
USE `SucursalBogota` ;

-- -----------------------------------------------------
-- Table `SucursalBogota`.`Datos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SucursalBogota`.`Datos` (
	`Id` INT auto_increment NOT NULL,
  `Ciudad` VARCHAR(60) NOT NULL,
  `Departamento` VARCHAR(60) NOT NULL,
  `Barrio` VARCHAR(60) NOT NULL,
  `EsNueva` VARCHAR(60) NOT NULL,
  `FechaConstruccion` VARCHAR(60) NOT NULL,
  `Area` VARCHAR(60) NULL,
  `TipoInmueble` VARCHAR(60) NOT NULL,
  `EstratoSocioEconomico` VARCHAR(60) NULL,
  `PrecioInicial` VARCHAR(60) NOT NULL,
  `ValorImpuestoPredial` VARCHAR(60) NOT NULL,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;

CREATE SCHEMA IF NOT EXISTS `SucursalValle` DEFAULT CHARACTER SET utf8 ;
USE `SucursalValle` ;

-- -----------------------------------------------------
-- Table `SucursalValle`.`Datos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SucursalValle`.`Datos` (
	`Id` INT auto_increment NOT NULL,
  `Ciudad` VARCHAR(60) NOT NULL,
  `Departamento` VARCHAR(60) NOT NULL,
  `Barrio` VARCHAR(60) NOT NULL,
  `EsNueva` VARCHAR(60) NOT NULL,
  `FechaConstruccion` VARCHAR(60) NOT NULL,
  `Area` VARCHAR(60) NULL,
  `TipoInmueble` VARCHAR(60) NOT NULL,
  `EstratoSocioEconomico` VARCHAR(60) NULL,
  `PrecioInicial` VARCHAR(60) NOT NULL,
  `ValorImpuestoPredial` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;
