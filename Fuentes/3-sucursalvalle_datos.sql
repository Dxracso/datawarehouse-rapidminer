-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sucursalvalle
-- ------------------------------------------------------
-- Server version	8.0.16

CREATE SCHEMA IF NOT EXISTS `SucursalBogota` DEFAULT CHARACTER SET utf8 ;
USE `SucursalBogota` ;

-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `SucursalValle` DEFAULT CHARACTER SET utf8 ;
USE `SucursalValle` ;

-- -----------------------------------------------------
-- Table `SucursalValle`.`Datos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SucursalValle`.`Datos` (
	`Id` INT auto_increment NOT NULL,
  `Ciudad` VARCHAR(60) NOT NULL,
  `Departamento` VARCHAR(60) NOT NULL,
  `Barrio` VARCHAR(60) NOT NULL,
  `EsNueva` VARCHAR(60) NOT NULL,
  `FechaConstruccion` VARCHAR(60) NOT NULL,
  `Area` VARCHAR(60) NULL,
  `TipoInmueble` VARCHAR(60) NOT NULL,
  `EstratoSocioEconomico` VARCHAR(60) NULL,
  `PrecioInicial` VARCHAR(60) NOT NULL,
  `ValorImpuestoPredial` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `datos`
--

DROP TABLE IF EXISTS `datos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `datos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Ciudad` varchar(60) NOT NULL,
  `Departamento` varchar(60) NOT NULL,
  `Barrio` varchar(60) NOT NULL,
  `EsNueva` varchar(60) NOT NULL,
  `FechaConstruccion` varchar(60) NOT NULL,
  `Area` varchar(60) DEFAULT NULL,
  `TipoInmueble` varchar(60) NOT NULL,
  `EstratoSocioEconomico` varchar(60) DEFAULT NULL,
  `PrecioInicial` varchar(60) NOT NULL,
  `ValorImpuestoPredial` varchar(60) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datos`
--

LOCK TABLES `datos` WRITE;
/*!40000 ALTER TABLE `datos` DISABLE KEYS */;
INSERT INTO `datos` VALUES (1,'CALI','VALLE DEL CAUCA','Rodrigo Lara Bonilla','false','22/05/1984','59','Casa','2','81780395','218308\r'),(2,'CALI','VALLE DEL CAUCA','El trebol','false','18/11/2011','57','Casa','4','90780395','254689\r'),(3,'CALI','VALLE DEL CAUCA','El jardin','false','14/10/1971','55','Casa','2','119780395','342039\r'),(4,'CALI','VALLE DEL CAUCA','Asturias','true','10/12/2019','54','Casa','1','127780395','356720\r'),(5,'CALI','VALLE DEL CAUCA','El jardin','true','28/10/2019','57','Apartamento','1','145780395','416688\r'),(6,'CALI','VALLE DEL CAUCA','Los Robles','true','26/07/2019','64','Casa','2','143780395','400188\r'),(7,'CALI','VALLE DEL CAUCA','Rodrigo Lara Bonilla','false','28/11/2006','60','Casa','4','136780395','384884\r'),(8,'CALI','VALLE DEL CAUCA','Los Robles','false','08/05/2008','65','Casa','3','157780395','451865\r'),(9,'CALI','VALLE DEL CAUCA','Asturias','true','09/04/2019','60','Casa','2','153780395','432293\r'),(10,'CALI','VALLE DEL CAUCA','El trebol','true','25/04/2019','50','Casa','2','163780395','440387\r'),(11,'CALI','VALLE DEL CAUCA','El trebol','false','23/07/1992','55','Casa','4','156780395','449872\r'),(12,'CALI','VALLE DEL CAUCA','El trebol','false','02/12/2001','60','Casa','3','178780395','490652\r'),(13,'CALI','VALLE DEL CAUCA','Asturias','false','01/01/1998','74','Casa','2','188780395','505511\r'),(14,'CALI','VALLE DEL CAUCA','Rodrigo Lara Bonilla','false','14/10/1971','70','duplex','4','194780395','515626\r'),(15,'CALI','VALLE DEL CAUCA','Rodrigo Lara Bonilla','false','01/11/1974','80','duplex','1','201780395','533597\r'),(16,'CALI','VALLE DEL CAUCA','Los Robles','false','02/05/1979','79','Casa','3','203780395','551905\r'),(17,'CALI','VALLE DEL CAUCA','Asturias','false','23/10/1978','76','Apartamento','3','217780395','610994\r'),(18,'CALI','VALLE DEL CAUCA','El trebol','true','29/12/2019','74','duplex','1','202780395','546943\r'),(19,'CALI','VALLE DEL CAUCA','Asturias','true','21/11/2019','75','Casa','1','222780395','638637\r'),(20,'CALI','VALLE DEL CAUCA','El jardin','false','18/04/1988','85','Casa','3','227780395','646010\r'),(21,'CALI','VALLE DEL CAUCA','El trebol','true','18/06/2019','90','duplex','1','245780395','680675\r'),(22,'CALI','VALLE DEL CAUCA','El trebol','true','21/11/2019','75','duplex','4','239780395','632753\r'),(23,'CALI','VALLE DEL CAUCA','Rodrigo Lara Bonilla','false','30/03/1970','105','Apartamento','2','281780395','797595\r'),(24,'CALI','VALLE DEL CAUCA','El jardin','false','21/04/2013','90','Apartamento','2','271780395','762494\r'),(25,'CALI','VALLE DEL CAUCA','El trebol','false','16/01/1978','87','Casa','2','276780395','731161\r'),(26,'CALI','VALLE DEL CAUCA','Rodrigo Lara Bonilla','true','16/08/2019','84','Apartamento','4','275780395','733116\r'),(27,'CALI','VALLE DEL CAUCA','Asturias','false','06/12/1973','110','Apartamento','1','302780395','827599\r'),(28,'CALI','VALLE DEL CAUCA','Asturias','false','09/12/1981','82','Apartamento','2','290780395','844070\r'),(29,'CALI','VALLE DEL CAUCA','Los Robles','false','10/03/2010','86','Apartamento','4','279780395','79659\r'),(30,'CALI','VALLE DEL CAUCA','El trebol','false','06/03/1992','90','Apartamento','1','283780395','780396');
/*!40000 ALTER TABLE `datos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-26 20:50:25
