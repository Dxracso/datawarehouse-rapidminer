-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sucursalbogota
-- ------------------------------------------------------
-- Server version	8.0.16

CREATE SCHEMA IF NOT EXISTS `SucursalBogota` DEFAULT CHARACTER SET utf8 ;
USE `SucursalBogota` ;

-- -----------------------------------------------------
-- Table `SucursalBogota`.`Datos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SucursalBogota`.`Datos` (
	`Id` INT auto_increment NOT NULL,
  `Ciudad` VARCHAR(60) NOT NULL,
  `Departamento` VARCHAR(60) NOT NULL,
  `Barrio` VARCHAR(60) NOT NULL,
  `EsNueva` VARCHAR(60) NOT NULL,
  `FechaConstruccion` VARCHAR(60) NOT NULL,
  `Area` VARCHAR(60) NULL,
  `TipoInmueble` VARCHAR(60) NOT NULL,
  `EstratoSocioEconomico` VARCHAR(60) NULL,
  `PrecioInicial` VARCHAR(60) NOT NULL,
  `ValorImpuestoPredial` VARCHAR(60) NOT NULL,
    PRIMARY KEY (`Id`))
ENGINE = InnoDB;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `datos`
--

DROP TABLE IF EXISTS `datos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `datos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Ciudad` varchar(60) NOT NULL,
  `Departamento` varchar(60) NOT NULL,
  `Barrio` varchar(60) NOT NULL,
  `EsNueva` varchar(60) NOT NULL,
  `FechaConstruccion` varchar(60) NOT NULL,
  `Area` varchar(60) DEFAULT NULL,
  `TipoInmueble` varchar(60) NOT NULL,
  `EstratoSocioEconomico` varchar(60) DEFAULT NULL,
  `PrecioInicial` varchar(60) NOT NULL,
  `ValorImpuestoPredial` varchar(60) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datos`
--

LOCK TABLES `datos` WRITE;
/*!40000 ALTER TABLE `datos` DISABLE KEYS */;
INSERT INTO `datos` VALUES (1,'BOGOTA, D.C.','BOGOTA','Jerusalen','false','22/05/1984','59','Casa','4','96348518','274860\r'),(2,'BOGOTA, D.C.','BOGOTA','Bosa Central','false','18/11/2011','57','Casa','4','105348518','295853\r'),(3,'BOGOTA, D.C.','BOGOTA','Jerusalen','false','14/10/1971','55','Casa','3','134348518','360128\r'),(4,'BOGOTA, D.C.','BOGOTA','Muzu','true','10/12/2019','54','Casa','2','142348518','39897\r'),(5,'BOGOTA, D.C.','BOGOTA','Tintal Norte','true','28/10/2019','57','Apartamento','2','160348518','433386\r'),(6,'BOGOTA, D.C.','BOGOTA','Chico Lago','true','26/07/2019','64','Casa','4','158348518','420503\r'),(7,'BOGOTA, D.C.','BOGOTA','Jerusalen','false','28/11/2006','60','Casa','3','151348518','423355\r'),(8,'BOGOTA, D.C.','BOGOTA','Chico Lago','false','08/05/2008','65','Casa','1','172348518','474437\r'),(9,'BOGOTA, D.C.','BOGOTA','Muzu','true','09/04/2019','60','Casa','4','168348518','486340\r'),(10,'BOGOTA, D.C.','BOGOTA','Bosa Central','true','25/04/2019','50','Casa','2','178348518','496898\r'),(11,'BOGOTA, D.C.','BOGOTA','Bosa Central','false','23/07/1992','55','Casa','2','171348518','480727\r'),(12,'BOGOTA, D.C.','BOGOTA','Bosa Central','false','02/12/2001','60','Casa','2','193348518','543524\r'),(13,'BOGOTA, D.C.','BOGOTA','Muzu','false','01/01/1998','74','Casa','2','203348518','560902\r'),(14,'BOGOTA, D.C.','BOGOTA','Jerusalen','false','14/10/1971','70','duplex','2','209348518','570474\r'),(15,'BOGOTA, D.C.','BOGOTA','Jerusalen','false','01/11/1974','80','duplex','2','216348518','617795\r'),(16,'BOGOTA, D.C.','BOGOTA','Chico Lago','false','02/05/1979','79','Casa','3','218348518','599851\r'),(17,'BOGOTA, D.C.','BOGOTA','Muzu','false','23/10/1978','76','Apartamento','2','232348518','630568\r'),(18,'BOGOTA, D.C.','BOGOTA','Bosa Central','true','29/12/2019','74','duplex','1','217348518','57959\r'),(19,'BOGOTA, D.C.','BOGOTA','Muzu','true','21/11/2019','75','Casa','4','237348518','667872\r'),(20,'BOGOTA, D.C.','BOGOTA','Tintal Norte','false','18/04/1988','85','Casa','1','242348518','676556\r'),(21,'BOGOTA, D.C.','BOGOTA','Muzu','true','18/06/2019','90','duplex','1','260348518','73259\r'),(22,'BOGOTA, D.C.','BOGOTA','Bosa Central','true','21/11/2019','75','duplex','3','254348518','729132\r'),(23,'BOGOTA, D.C.','BOGOTA','Tintal Norte','false','30/03/1970','105','Apartamento','2','296348518','855294\r'),(24,'BOGOTA, D.C.','BOGOTA','Bosa Central','false','21/04/2013','90','Apartamento','3','286348518','794617\r'),(25,'BOGOTA, D.C.','BOGOTA','Bosa Central','false','16/01/1978','87','Casa','4','291348518','799589\r'),(26,'BOGOTA, D.C.','BOGOTA','Tintal Norte','true','16/08/2019','84','Apartamento','3','290348518','779908\r'),(27,'BOGOTA, D.C.','BOGOTA','Muzu','false','06/12/1973','110','Apartamento','1','317348518','855959\r'),(28,'BOGOTA, D.C.','BOGOTA','Muzu','false','09/12/1981','82','Apartamento','4','305348518','855824\r'),(29,'BOGOTA, D.C.','BOGOTA','Chico Lago','false','10/03/2010','86','Apartamento','4','294348518','802099\r'),(30,'BOGOTA, D.C.','BOGOTA','Bosa Central','false','06/03/1992','90','Apartamento','3','298348518','868525');
/*!40000 ALTER TABLE `datos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-26 20:50:14
